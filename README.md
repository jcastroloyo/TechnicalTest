# TechnicalTest
This repository contains the four projects that make up the architecture developed for the GBTech technical test.

## Environment
The environment must have Java and mongodb installed. By default the mongo port is 27017, if you want
to modify the port for email services, you must modify the application.properties located in the TechnicalTest
project.

## Installation instructions
Once the repository has been cloned, we must generate the .jar using the command.
```
gradlew bootjar
```
Once the .jar has been generated, we execute it using:
```
 java -jar {name.jar} 
```
It is important to get them up in the following order:
1. Eureka-server
2. Users and emails services
3. Gateway

## The components will be deployed in:

    eureka-server – localhost:8761
    emails-services – localhost:8989
    users-services – localhost:8988
    gatewat – localhost:8080

Recall that the API documentation is not mapped to the gateway, so it must be accessed:

    emails-services – http://localhost:8989/swagger-ui/index.html
    users-services – http://localhost:8988/swagger-ui/index.html
    
Clearly , access to services in a real environment should be done only from the gateway, having them in
a private network.
