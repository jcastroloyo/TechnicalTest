package com.gbtech.dummyUsers.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.gbtech.dummyUsers.services.UsersService;

@RestController
@RequestMapping("/users")
public class UsersController {
    private final UsersService service;
    @Autowired
    public UsersController(UsersService service) {
        this.service = service;
    }
    @PostMapping(path = "/login")
    public ResponseEntity<String> login(@RequestBody String user) {
        return service.dummyLogIn(user);
    }
    @GetMapping(path = "")
    public ResponseEntity<String> getUser() {
        return service.dummyGetUser("Example User");
    }
}
