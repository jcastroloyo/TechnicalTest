package com.gbtech.dummyUsers.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UsersService {

    @Autowired
    public UsersService() {

    }

    public ResponseEntity<String> dummyLogIn(String user) {
        return ResponseEntity.status(200).body(user + " is logged in");
    }

    public ResponseEntity<String> dummyGetUser(String user) {
        return ResponseEntity.status(200).body("Logged user is "+user);
    }
}
