package com.gbtech.dummyUsers;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/*In this project we would have the services related to user management.
 This project only includes doomie calls that will serve as an illustration
 to show the composition of microservices.*/
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@OpenAPIDefinition(info =
	@Info(title = "Users API", version = "1.0", description = "Access to de emails service users API v1.0")
)
public class DummyUsersApplication {

	public static void main(String[] args) {
		SpringApplication.run(DummyUsersApplication.class, args);
	}

}
