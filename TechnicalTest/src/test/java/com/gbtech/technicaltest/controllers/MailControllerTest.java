package com.gbtech.technicaltest.controllers;

import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.services.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.MimeTypeUtils;

import java.util.List;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


/*
We can see an example of unit tests on the MailController,
for temporary reasons not all tests have been performed,
but this class serves to illustrate how we would perform them.
* */
@WebMvcTest(MailController.class)
class MailControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private EmailService service;

    /*@WebMvcTest is having problems with @EnableMongoAuditing,
    we need to muke this class for the correct functioning of the tests.
     */
    @MockBean
    private MappingMongoConverter mappingMongoConverter;

    @MockBean
    private Email aux;

    @Test
    void getEmail() throws Exception {
        given(service.getEmail(null,null,null,null)).willReturn(new ResponseEntity<List<Email>>(HttpStatus.OK));
        var getEmails = mockMvc.perform(
                        get("/emails").accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andReturn();
    }

}