package com.gbtech.technicaltest.services;

import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.repository.Emails;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import static org.mockito.Mockito.when;

/*
We can see an example of unit tests on the EmailService,
for temporary reasons not all tests have been performed,
but this class serves to illustrate how we would perform them.
* */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmailServiceImplTest {

    @Autowired
    private EmailService service;

    //We can create a mock of entire "layer" of our application
    @MockBean
    private Emails db;

    private Email auxEmail;

    //Test for the method of obtaining an email by id
    @Test
    void getEmail() {
        Optional<Email> aux = Optional.of(createEmail());
        when(db.findOneByEmailId("20")).thenReturn(aux);
        assert Objects.requireNonNull(service.getEmail("20").getBody()).getEmailBody().equals("Email test content");
    }

    //Test for the method of obtaining all e-mails
    @Test
    void testGetEmail() {
        when(db.findAll()).thenReturn(List.of());
    }

    @Test
    void saveEmail() {
        Email aux = createEmail();
        when(db.save(aux)).thenReturn(aux);
        assert Objects.equals(service.saveEmail(aux).getBody(), "Email saved");
    }

    private Email createEmail() {
        Email email = new Email();
        email.setEmailId("20");
        email.setEmailBody("Email test content");
        email.setEmailFrom("Email test from");
        return email;
    }
}