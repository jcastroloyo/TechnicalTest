package com.gbtech.technicaltest;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.mongodb.config.EnableMongoAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;

/*If it were a more complex system, we would add a configuration server that would allow us
to centralise the configurations of the entire system in a single point,
facilitating management and hot changes. -dependency-> spring-cloud-config-server
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableScheduling
@EnableMongoAuditing
@EnableFeignClients
@OpenAPIDefinition(info =
  @Info(title = "Emails API", version = "1.0", description = "Emails CRUD operations API v1.0")
)
public class TechnicalTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechnicalTestApplication.class, args);
	}

}
