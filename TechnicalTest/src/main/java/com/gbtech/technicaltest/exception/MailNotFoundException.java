package com.gbtech.technicaltest.exception;

//Exception when a specific email is not found
public class MailNotFoundException extends RuntimeException {
    public MailNotFoundException(String id) {
        super(String.format("Mail with Id %d not found", id));
    }
}
