package com.gbtech.technicaltest.exception;

//Exception when modifying an email with a status other than DRAFT.
public class InvalidStatusException extends RuntimeException {
    public InvalidStatusException(String id) {
        super(String.format("Only DRAFT emails could be updated. Email: ", id));
    }
}