package com.gbtech.technicaltest.exception;

//Exception when a DB collection is empty
public class EmptyException extends RuntimeException{
    public EmptyException() {
        super("No data found on BD");
    }
}
