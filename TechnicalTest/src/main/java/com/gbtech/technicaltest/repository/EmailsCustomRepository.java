package com.gbtech.technicaltest.repository;

import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.model.State;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.result.UpdateResult;

import java.util.List;

public interface EmailsCustomRepository {
    List<Email> findEmailsByCriteria(String emailFrom, String emailTo, String emailCC, State state);
    BulkWriteResult deleteEmailsByCriteria(String emailFrom, String emailTo, String emailCC, State state);
    BulkWriteResult saveMultipleEmails(List<Email> emails);
    UpdateResult markAsSpam();
    UpdateResult updateEmail(String id, State state, String content);
}
