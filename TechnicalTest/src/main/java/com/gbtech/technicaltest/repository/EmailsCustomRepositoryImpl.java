package com.gbtech.technicaltest.repository;

import com.gbtech.technicaltest.exception.InvalidStatusException;
import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.model.State;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.result.UpdateResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

//We use mongoTemplate to
@Repository
public class EmailsCustomRepositoryImpl implements EmailsCustomRepository{
    @Autowired
    MongoTemplate mongoTemplate;

    /*We can obtain a list of the mails that respond to the filters applied based on the method parameters:
        emailFrom --> String indicating who is sending the email
        emailTo --> String to be searched in the recipient list
        emailCC --> String to be searched in the CC list
        state --> State of the email {DRAFT, SENT, DELETED, SPAM}
     */
    @Override
    public List<Email> findEmailsByCriteria(String emailFrom, String emailTo, String emailCC, State state){
        final Query query = new Query();
        if(emailFrom!=null) query.addCriteria(Criteria.where("emailFrom").is(emailFrom));
        if(emailTo!=null) query.addCriteria(Criteria.where("emailTo").in(emailTo));
        if(emailCC!=null) query.addCriteria(Criteria.where("emailCC").in(emailCC));
        if(state!=null) query.addCriteria(Criteria.where("state").is(state));
        return mongoTemplate.find(query,Email.class);
    }

    /*We can obtain a list of the mails that respond to the filters applied based on the method parameters:
        emailFrom --> String indicating who is sending the email
        emailTo --> String to be searched in the recipient list
        emailCC --> String to be searched in the CC list
        state --> State of the email {DRAFT, SENT, DELETED, SPAM}
     */
    @Override
    public BulkWriteResult deleteEmailsByCriteria(String emailFrom, String emailTo, String emailCC, State state) {
        BulkOperations bulkOperations = this.mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, "email");
        final Query query = new Query();
        if(emailFrom!=null) query.addCriteria(Criteria.where("emailFrom").is(emailFrom));
        if(emailTo!=null) query.addCriteria(Criteria.where("emailTo").in(emailTo));
        if(emailCC!=null) query.addCriteria(Criteria.where("emailCC").in(emailCC));
        if(state!=null) query.addCriteria(Criteria.where("state").is(state));
        bulkOperations.remove(query);
        return bulkOperations.execute();
    }

    //Save a list of emails
    @Override
    public BulkWriteResult saveMultipleEmails(List<Email> emails) {
        BulkOperations bulkOperations = this.mongoTemplate.bulkOps(BulkOperations.BulkMode.UNORDERED, "email");
        bulkOperations.insert(emails);
        return bulkOperations.execute();
    }

    /*
    Search for all emails sent by "carl@gbtec.com" and change state=SPAM
    Mark as transactional to evitate race conditions
    */
    @Override
    @Transactional
    public UpdateResult markAsSpam() {
        final Query query = new Query();
        query.addCriteria(Criteria.where("emailFrom").is("carl@gbtec.com"));
        Update update = new Update();
        update.set("state", State.SPAM);
        return mongoTemplate.updateMulti(query, update, Email.class);
    }


    @Override
    public UpdateResult updateEmail(String id, State state, String content) {
        final Query query = new Query();
        query.addCriteria(Criteria.where("emailId").is(id));
        Email auxE = mongoTemplate.findOne(query,Email.class);
        if(auxE.getState()!= State.DRAFT) throw new InvalidStatusException(id);
        else{
            Update update = new Update();
            update.set("state", state);
            update.set("emailBody", content);
            return mongoTemplate.updateFirst(query, update, Email.class);
        }
    }
}
