package com.gbtech.technicaltest.repository;
import com.gbtech.technicaltest.model.Email;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Optional;

@Repository
public interface Emails extends MongoRepository<Email, String>, EmailsCustomRepository {
    @NotNull
    Optional<Email> findOneByEmailId(@NotNull String emailId);
    @NotNull
    List<Email> findAll();
    @NotNull
    Email save(Email entity);
}

