package com.gbtech.technicaltest.controllers;
import com.gbtech.technicaltest.model.BulkEmails;
import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.model.State;
import com.gbtech.technicaltest.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/emails")
public class MailController {
    private final EmailService service;

    @Autowired
    public MailController(EmailService service) {
        this.service=service;
    }

    //GETTERS--> From ID and from diferent criteria
    @GetMapping(path = "/{id}")
    public ResponseEntity<Email> getEmail(@PathVariable("id") String id) {
        return service.getEmail(id);
    }

    @GetMapping(
            path = "",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<List<Email>> getEmails(
            @RequestParam(required = false) String emailFrom,
            @RequestParam(required = false) String emailTo,
            @RequestParam(required = false) String emailCC,
            @RequestParam(required = false) State state) {
        return service.getEmail(emailFrom, emailTo, emailCC, state);
    }

    //POST for create one email
    @PostMapping(
            path = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<String> saveEmail(@Valid @RequestBody Email email) {
         return service.saveEmail(email);
    }

    //POST for create a list of emails
    //I know this is not good practice in REST, but I can't find a better solution. Maybe with a multipart?
    @PostMapping(
            path = "/list",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<String> saveEmails(@Valid @RequestBody BulkEmails emails) {
        return service.saveEmails(emails.getBulk());
    }


    //DELETE--> From ID and from diferent criteria
    @DeleteMapping(
            path = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<String> deleteEmail(@PathVariable("id") String id) {
        return  service.delete(id);
    }

    @DeleteMapping(
            path = "",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<String> deleteEmails(@RequestParam(required = false) String emailFrom,
                                                @RequestParam(required = false) String emailTo,
                                                @RequestParam(required = false) String emailCC,
                                                @RequestParam(required = false) State state) {
        return  service.delete(emailFrom, emailTo, emailCC, state);
    }

    //PUT--> From ID, can update content and state
    @PutMapping(
            path = "/{id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    public ResponseEntity<String> updateEmail(
            @PathVariable String id,
            @Validated @RequestBody Email email) {
        return service.updateEmail(id, email);
    }
}
