package com.gbtech.technicaltest.model;

public enum State{DRAFT, SENT, DELETED, SPAM}