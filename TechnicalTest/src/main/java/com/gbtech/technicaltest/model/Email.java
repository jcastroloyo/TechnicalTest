package com.gbtech.technicaltest.model;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Data
@Document
public class Email {
    @Id
    private String emailId;
    @NotNull
    @Indexed
    private String emailFrom;
    private List<String> emailTo;
    private List<String> emailCC;
    private String emailBody;
    @Indexed
    private State state;

    @LastModifiedDate
    private Date modifiedAt;

    /* If we wanted to associate a user to an email that we have, for example,
    in another distributed database that we access through the user microservice,
    we would annotate it with @Transient so that it is not saved in the database and the information is not replicated.
    private User user;*/

    public Date getModifiedAt() {
        return modifiedAt;
    }

    public void setModifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getEmailFrom() {
        return emailFrom;
    }

    public void setEmailFrom(String emailFrom) {
        this.emailFrom = emailFrom;
    }

    public List<String> getEmailTo() {
        return emailTo;
    }

    public void setEmailTo(List<String> emailTo) {
        this.emailTo = emailTo;
    }

    public List<String> getEmailCC() {
        return emailCC;
    }

    public void setEmailCC(List<String> emailCC) {
        this.emailCC = emailCC;
    }

    public String getEmailBody() {
        return emailBody;
    }

    public void setEmailBody(String emailBody) {
        this.emailBody = emailBody;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
