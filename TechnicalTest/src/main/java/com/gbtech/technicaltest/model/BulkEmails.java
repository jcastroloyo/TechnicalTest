package com.gbtech.technicaltest.model;
import lombok.Data;
import java.util.List;

@Data
public class BulkEmails<T> {
        private List<Email> bulk;

    public List<Email> getBulk() {
        return bulk;
    }

    public void setBulk(List<Email> bulk) {
        this.bulk = bulk;
    }
}
