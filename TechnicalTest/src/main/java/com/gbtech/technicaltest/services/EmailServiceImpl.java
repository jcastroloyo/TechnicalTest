package com.gbtech.technicaltest.services;

import com.gbtech.technicaltest.client.UsersClient;
import com.gbtech.technicaltest.exception.EmptyException;
import com.gbtech.technicaltest.exception.MailNotFoundException;
import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.model.State;
import com.gbtech.technicaltest.repository.Emails;
import com.mongodb.client.result.UpdateResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmailServiceImpl implements EmailService{
    private final Emails db;
    private final UsersClient uc;
    private static final Logger log = LoggerFactory.getLogger(EmailService.class);

    @Autowired
    public EmailServiceImpl(Emails db, UsersClient uc) {
        this.db=db;
        this.uc=uc;
    }

    public ResponseEntity<Email> getEmail(String id) {
        /*
       To illustrate an example of service composition, we will say that before getting an email
       we automatically log in the user and get the logged in user.
        */
        log.info(uc.login("Example User").getBody());
        log.info(uc.getUser().getBody());
        return db.findOneByEmailId(id).map(email -> ResponseEntity.status(200).body(email)).orElseThrow(() -> new MailNotFoundException(id));
    }

    public ResponseEntity<List<Email>> getEmail(String emailFrom, String emailTo, String emailCC, State state) {
        List<Email> emails = db.findEmailsByCriteria(emailFrom, emailTo, emailCC, state);
        if(emails.isEmpty()) throw new EmptyException();
        return ResponseEntity.status(200).body(emails);
    }

    public ResponseEntity<String> saveEmail(Email email) {
       db.save(email);
       return ResponseEntity.status(HttpStatus.CREATED).body("Email saved");
    }

    public ResponseEntity<String> saveEmails(List<Email> emails) {
        if(emails!=null) return ResponseEntity.status(HttpStatus.CREATED).body(db.saveMultipleEmails(emails).getInsertedCount()+" saved");
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("You have to set at least one email");
    }

    public ResponseEntity<String> delete(String id) {
        db.deleteById(id);
        return ResponseEntity.status(HttpStatus.OK).body("Email with id: "+id+ " deleted");
    }

    public ResponseEntity<String> delete(String emailFrom, String emailTo, String emailCC, State state) {
        return ResponseEntity.status(HttpStatus.OK).body(db.deleteEmailsByCriteria(emailFrom, emailTo, emailCC, state).getDeletedCount() + " deleted");
    }

    public ResponseEntity<String> updateEmail(String id, Email email) {
        return ResponseEntity.status(201).body(db.updateEmail(id, email.getState(),email.getEmailBody()).getModifiedCount() + " updated");
    }

    public UpdateResult markAsSpam(){
        return db.markAsSpam();
    }
}
