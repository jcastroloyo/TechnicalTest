package com.gbtech.technicaltest.services;

import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.model.State;
import com.mongodb.client.result.UpdateResult;
import org.springframework.http.ResponseEntity;

import java.util.List;


public interface EmailService {
    ResponseEntity<Email> getEmail(String id);
    ResponseEntity<List<Email>> getEmail(String emailFrom, String emailTo, String emailCC, State state);
    ResponseEntity<String> saveEmail(Email email);
    ResponseEntity<String> saveEmails(List<Email> emails);
    ResponseEntity<String> delete(String id);
    ResponseEntity<String> delete(String emailFrom, String emailTo, String emailCC, State state);
    ResponseEntity<String> updateEmail(String id, Email email);
    UpdateResult markAsSpam();
}
