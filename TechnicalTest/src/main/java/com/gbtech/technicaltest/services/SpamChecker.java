package com.gbtech.technicaltest.services;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
    @Component
    public class SpamChecker {
        private final EmailService sp;
        public SpamChecker(EmailService sp) {
            this.sp=sp;
        }
        //We set up a log erxample with SLF4J, we could use the tag too
        private static final Logger log = LoggerFactory.getLogger(SpamChecker.class);
        private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

        /*We use Cron Expressions to specify that the task will be executed at 10 o’clock of every day.
        We could do it asynchronously if we wanted to using the @Async annotation and create a TaskExecutor,
        We chose not to do it in order to avoid racing conditions.
        * */
        @Scheduled(cron = "0 0 10 * * *")
        public void markAsSpam() {
            log.info("Archiving spam: "+  sp.markAsSpam().getModifiedCount() +"mails "+ dateFormat.format(new Date()));
        }
}

