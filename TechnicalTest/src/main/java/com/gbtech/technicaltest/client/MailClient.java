package com.gbtech.technicaltest.client;
import com.gbtech.technicaltest.model.BulkEmails;
import com.gbtech.technicaltest.model.Email;
import com.gbtech.technicaltest.model.State;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@FeignClient(name = "email.service", path = "/emails")
public interface MailClient {
    @GetMapping(path = "/{id}")
    ResponseEntity<Email> getEmail(@PathVariable("id") String id);

    @GetMapping(
            path = "",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    ResponseEntity<List<Email>> getEmails(
            @RequestParam(required = false) String emailFrom,
            @RequestParam(required = false) String emailTo,
            @RequestParam(required = false) String emailCC,
            @RequestParam(required = false) State state);

    @PostMapping(
            path = "",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    ResponseEntity<String> saveEmail(@Valid @RequestBody Email email);

    @PostMapping(
            path = "/list",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    ResponseEntity<String> saveEmails(@Valid @RequestBody BulkEmails emails);

    @DeleteMapping(
            path = "/{id}",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    ResponseEntity<String> deleteEmail(@PathVariable("id") String id);

    @DeleteMapping(
            path = "",
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    ResponseEntity<String> deleteEmails(@RequestParam(required = false) String emailFrom,
                                               @RequestParam(required = false) String emailTo,
                                               @RequestParam(required = false) String emailCC,
                                               @RequestParam(required = false) State state);
    @PutMapping(
            path = "/{id}",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE}
    )
    ResponseEntity<String> updateEmail(
            @PathVariable String id,
            @Validated @RequestBody Email email);


}

