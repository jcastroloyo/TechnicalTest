package com.gbtech.technicaltest.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



/*
In the GET of an email (EmailServiceImpl) a test call is made to
the user management microservice to illustrate how it works.
 */
@FeignClient(name="users.service", path = "/users")
public interface UsersClient {
    @PostMapping(path = "/login")
    ResponseEntity<String> login(@RequestBody String user);

    @GetMapping(path = "")
    ResponseEntity<String> getUser();
}
